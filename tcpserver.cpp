//
// Created by ostapvad on 06.05.23.
//

#include "tcpserver.h"



void TCPServer::run(unsigned int totalSeconds) {
	acceptConnection();

	// Run the IO context for the specified duration
	std::chrono::seconds duration(totalSeconds);
	io_context_.run_for(duration);

	// Stop the IO context
	io_context_.stop();
}

TCPServer::TCPServer(unsigned short port)
		: io_context_(),
		  port_(port),
		  acceptor_(io_context_, boost::asio::ip::tcp::endpoint(boost::asio::ip::tcp::v6(), port_)),
		  graph_(new Graph()) {
}

void TCPServer::acceptConnection() {
	// Create a new session and initiate asynchronous accept
	auto session = std::make_shared<Session>(io_context_.get_executor(), graph_);
	acceptor_.async_accept(session->socket, [this, session](const boost::system::error_code& error) {
		if (error)
			return;

		// Start handling the session
		session->startHandling();

		// Accept another connection
		acceptConnection();
	});
}

TCPServer::~TCPServer() {
	// Clean up the graph object
	delete graph_;

	// Close the acceptor if it's open
	if (acceptor_.is_open()) {
		acceptor_.close();
	}

	std::cout << "Server is stopped" << std::endl;
}
