cmake_minimum_required(VERSION 3.18)
project(esw_efficient_servers)

SET(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++20 -Ofast")
find_package(Protobuf REQUIRED)

find_package(Boost 1.42.0 REQUIRED COMPONENTS system thread regex iostreams)

# Add the path to your .proto file(s)
set(PROTO_FILES proto/server.proto)

# Generate the .pb.cc and .pb.h files
protobuf_generate_cpp(PROTO_SRCS PROTO_HDRS ${PROTO_FILES})

# Include the generated files in the project
include_directories(${CMAKE_CURRENT_BINARY_DIR})
include_directories(${TBB_INCLUDE_DIRS})
include_directories(${Boost_INCLUDE_DIRS})

add_executable(esw_efficient_servers
        main.cpp tcpserver.cpp session.cpp graph.cpp  ${PROTO_SRCS} ${PROTO_HDRS})
target_include_directories(esw_efficient_servers PRIVATE "${CMAKE_CURRENT_BINARY_DIR}")
target_link_libraries(esw_efficient_servers PRIVATE protobuf::libprotobuf ${Boost_LIBRARIES})

