# Async TCP Server for Shortest Drivable Distances

This project is an implementation of a single-thread asynchronous TCP server in C++ using Boost that solves 
the task of determining the shortest drivable distances between locations in a foreign city 
https://esw.pages.fel.cvut.cz/labs/efficient-servers/. The server handles multiple clients requests concurrently and 
provides efficient and scalable solutions to the problem.

## Features

- Single-threaded asynchronous TCP server implementation
- Efficient handling of multiple clients requests
- Scalable architecture for handling large volumes of data
- Support for concurrent processing of client requests
- Integration with the Boost.Asio library for asynchronous networking operations
- Utilization of the Graph data structure for efficient distance calculations
- Usage of protobuf

## Requirements

- C++ compiler with C++20 support.
- CMake (optional if using CLion).
- Boost.Asio library.
- Protobuf compiler.
- Other dependencies in CmakeLists.txt.
## Usage

### Cloning the Repository
git clone https://gitlab.fel.cvut.cz/ostapvad/esw-efficient-servers.git

### Building with CLion

1. Open the project in CLion.
2. Build the project using the built-in CMake build system.
3. Run the TCP server executable (main.cpp).

### Manual Compilation

1. Install the required dependencies: Boost.Asio library.
2. Open a terminal and navigate to the project directory.
3. Create a build directory:
```bash
cmake .
make
./esw_efficient_servers
```
### How it works
1. The server will start listening for clients connections on the default port.
2. Clients can connect to the server and send the desired requests
3. After expiring of time limit defined in main.cpp stops.
## Contributors
- Vadym Ostapovych ostapvad@fel.cvut.cz



