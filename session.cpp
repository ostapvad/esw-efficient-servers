//
// Created by ostapvad on 08.05.23.
//

#include "session.h"

uint32_t bigEndianToUnsignedInt(const char* bytes){
	uint32_t num = 0;
	num |= (unsigned char)(bytes[0]);
	num |= (unsigned char)(bytes[1]) << 8;
	num |= (unsigned char)(bytes[2]) << 16;
	num |= (unsigned char)(bytes[3]) << 24;

	boost::endian::big_to_native_inplace(num);
	return num;

}

void unsignedIntToBigEndianBytes(unsigned int value, char* bytes) {
	boost::endian::little_to_native_inplace(value);
	bytes[0] = (char)(value >> 24 );
	bytes[1] = (char)(value >> 16),
	bytes[2] = (char)(value >> 8),
	bytes[3] = (char)(value);
}


Session::Session(boost::asio::io_context::executor_type executor, Graph *graph): socket(std::move(executor)), graph_(graph){
}

void Session::startHandling() {
	msgBuffer_.resize(sizeof(uint32_t));
	// reads message size and sends response
	boost::asio::async_read(socket,
					 boost::asio::buffer(msgBuffer_, sizeof(uint32_t)),
					 [self = shared_from_this(), this](const boost::system::error_code &ec, std::size_t bytes_transferred){

						 if (!(ec.value() == 0 && bytes_transferred == sizeof(uint32_t))){
							 // new message was not arrived

							 return ;
						 }
						 parseRequest(ec);

					 }
	);
}

void Session::parseRequest(const boost::system::error_code& ec) {
	// Extract the message size from the received buffer in big-endian format
	uint32_t message_size = bigEndianToUnsignedInt(msgBuffer_.data());

	// Resize the buffer to accommodate the message data
	msgBuffer_.resize(message_size);

	esw::Response response;

	try {
		// Read the message data from the socket
		boost::asio::read(socket, boost::asio::buffer(msgBuffer_), boost::asio::transfer_exactly(message_size));
		esw::Request request;
		if (!request.ParseFromArray(msgBuffer_.data(), message_size)) {
			throw std::logic_error("MESSAGE was not parsed!");
		}

		// Resolve the client's request and populate the response object
		resolveRequest(request, response);
	} catch (std::exception& exception) {

		std::cout << "Exception occurred: " << exception.what();

		// Set error status and error message in the response
		response.set_status(esw::Response_Status_ERROR);
		response.set_errmsg(exception.what());
	}

	// Send the response back to the client
	sendResponse(response);

	// Start handling the next request
	startHandling();
}

void Session::resolveRequest(const esw::Request& request, esw::Response& response) {
	if (request.has_walk()) {
		// Handle a "walk" request

		// Add the walk to the graph
		graph_->AddWalk(request.walk());

		// Set the response status to OK
		response.set_status(esw::Response_Status_OK);
	} else if (request.has_onetoone()) {
		// Handle a "one-to-one" request
		std::cout << "One to one\n";

		// Get the origin and destination locations from the request
		auto dest = request.onetoone().destination();

		// Compute the shortest distance between the origin and destination
		long long int shortest_dist = graph_->computeDists(request.onetoone().origin(), &dest);
		std::cout << shortest_dist << std::endl;

		// Set the response status to OK and the shortest path length
		response.set_status(esw::Response_Status_OK);
		response.set_shortest_path_length(shortest_dist);
	} else if (request.has_onetoall()) {
		// Handle a "one-to-all" request
		std::cout << "One to all\n";

		// Compute the total length from the origin to all other vertices
		long long int total_length = graph_->computeDists(request.onetoall().origin(), nullptr);

		std::cout << total_length << std::endl;

		// Set the response status to OK and the total length
		response.set_status(esw::Response_Status_OK);
		response.set_total_length(total_length);
	} else if (request.has_reset()) {
		// Handle a "reset" request
		std::cout << "Reset\n";
		graph_ = new Graph();

		// Set the response status to OK
		response.set_status(esw::Response_Status_OK);
	} else {
		// Invalid request
		std::cout << "Wrong request!\n";

		// Set the response status to ERROR and an error message
		response.set_status(esw::Response_Status_ERROR);
		response.set_errmsg("Invalid request");
	}
}

void Session::sendResponse(const esw::Response& response) {
	// Get the size of the response message
	size_t response_msg_size = response.ByteSizeLong();

	// Create an array to store the response message data
	char response_byte_array[response_msg_size];

	// Create a buffer to store the message size
	char msg_size_buffer[4];
	response.SerializeToArray(response_byte_array, response_msg_size);

	// Convert the response message size to big endian and store it in the size buffer
	unsignedIntToBigEndianBytes(response_msg_size, msg_size_buffer);

	// Write message
	boost::asio::write(socket, boost::asio::buffer(msg_size_buffer, 4));
	boost::asio::write(socket, boost::asio::buffer(response_byte_array, response_msg_size));
}

Session::~Session() {
	// Check if the socket is open
	if(socket.is_open()) {
		socket.close();
	}
}






