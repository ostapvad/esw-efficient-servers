//
// Created by ostapvad on 07.05.23.
//

#ifndef ESW_EFFICIENT_SERVERS_GRAPH_H
#define ESW_EFFICIENT_SERVERS_GRAPH_H
#include "server.pb.h"

#include <queue>
#include <utility>
#include <numeric>
#include <cmath>
#include <iostream>
#define  SQUARE_THRESHOLD 250000
#define UNFOUND (-1)
#define TOTAL_DIRS 9

const long long int INF_LONG = std::numeric_limits<long long int>::max(); // INFINITY
// Map definitions
const int stepSize = 500;
const int mapSize = 1000000;
// Directions to search for nn
const int dirs[TOTAL_DIRS][2] = {
		{0, 0}, {-1, -1}, {-1, 0},
		{-1, 1}, {0, -1}, {0, 1},
		{1, -1}, {1, 0}, {1, 1}
};
// Pair for priority queue in Dijkstra
typedef std::pair<long long int, long long int> iPair;
// Definition of edge that can occur in many walks
struct Edge{
	unsigned int totalLength;
	unsigned int totalWalks;
};

/**
 * Calculates the squared Euclidean distance between two points.
 *
 * @param point1 The first point.
 * @param point2 The second point.
 * @return The squared Euclidean distance between the two points.
 */
inline long long int squaredEuclideanDistance(const esw::Location& point1, const esw::Location& point2);

/**
 * The Graph class represents a graph data structure used to save information about Walks
 */
class Graph {
public:
	/**
	 * Default constructor for the Graph class, initializes the totalWalks and totalNodes members.
	 */
	Graph();
	/**
	 * Computes the distances using Dijkstra, calculates shortest_path_length or total_length
	 *
	 *
	 * @param source The source location.
	 * @param target The target location (optional).
	 * @return The computed distances.
	 */
	long long int computeDists(const esw::Location& source, esw::Location *target);
	/**
	 * Prints the adjacency list of the graph(optional).
	 */
	void print_adj_list();
	/**
	 * Adds a walk to the graph and its private member variables
	 *
	 * @param walk The walk to be added.
	 */
	void AddWalk(const esw::Walk&);

private:
	/**
	 * Gets the vertex ID of a given location.
	 *
	 * @param location The location.
	 * @return The vertex ID if found, otherwise UNFOUND.
	 */
	int  getVertexID(const esw::Location &location);
	/**
	 * Sets the location in the graph with the corresponding vertex ID.
	 *
	 * @param location The location.
	 * @param vertexID The vertex ID.
	 */
	void setLocation(const esw::Location & location, int vertexID);
	// Private member variables
	// The adjacency list representing the graph: adj_list[from_vertex][to_vertex]
	std::map<long long int, std::map<long long int, Edge>> adj_list;
	// The adjacency list representing the graph: index(vertexID) -> (x,y)
	std::vector<esw::Location> vertex_to_loc;
	// Mapping of grid coordinates to vertex IDs: vertexID -> gridMap(x,y)
	std::map<int, std::map<int, int>>mapToVertexID;
	int totalNodes;     // Total number of nodes in the graph
	int totalWalks;     // Total number of walks added to the graph
	// identifies if there is a point (x, y)
	bool x_axis[mapSize]{};  // Flags for x-axis grid coordinates
	bool y_axis[mapSize]{};  // Flags for y-axis grid coordinates
};


#endif //ESW_EFFICIENT_SERVERS_GRAPH_H
