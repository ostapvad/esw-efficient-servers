//
// Created by ostapvad on 08.05.23.
//

#ifndef ESW_EFFICIENT_SERVERS_SESSION_H
#define ESW_EFFICIENT_SERVERS_SESSION_H

#include <boost/array.hpp>
#include "boost/asio.hpp"
#include <boost/endian/conversion.hpp>
#include "server.pb.h"
#include "graph.h"

/**
 * Converts a 4-byte big-endian representation to an unsigned integer.
 *
 * @param bytes A pointer to the array of 4 bytes in big-endian order.
 * @return The converted unsigned integer value.
 */
uint32_t bigEndianToUnsignedInt(const char* bytes);
/**
 * Converts an unsigned integer value to a 4-byte big-endian representation.
 *
 * @param value The unsigned integer value to be converted.
 * @param bytes A pointer to the array of 4 bytes to store the big-endian representation.
 */
void unsignedIntToBigEndianBytes(unsigned int value, char* bytes);

/**
 * The Session class represents a session between a client and the server.
 * It is responsible for handling client requests and sending responses.
 */
class Session : public std::enable_shared_from_this<Session> {
public:
	/**
	 * Constructs a Session object with the given executor and graph.
	 *
	 * @param executor The executor for asynchronous operations.
	 * @param graph A pointer to the Graph object.
	 */
	Session(boost::asio::io_context::executor_type executor, Graph* graph);

	/**
	 * Destructor for the Session class, closes the socket if necessary
	 */
	~Session();

	/**
	 * Starts handling the session by initiating asynchronous operations, reads and writes
	 */
	void startHandling();

	boost::asio::ip::tcp::socket socket; // The socket for communication with the client

private:
	std::vector<char> msgBuffer_; // Buffer for storing received message data
	Graph* graph_; // Pointer to the Graph object

	/**
	 * Parses the client's request to protobuf format
	 *
	 * @param ec The error code of the asynchronous operation.
	 */
	void parseRequest(const boost::system::error_code& ec);

	/**
	 * Resolves the client's protobuf request and prepares the response.
	 *
	 * @param request The client's request.
	 * @param response The response to be sent back to the client.
	 */
	void resolveRequest(const esw::Request& request, esw::Response& response);

	/**
	 * Sends the response to the client.
	 *
	 * @param response The response to be sent.
	 */
	void sendResponse(const esw::Response& response);
};


#endif //ESW_EFFICIENT_SERVERS_SESSION_H
