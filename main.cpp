#include "tcpserver.h"

#define DefaultPort 1234
#define TimeLimit 100

int main() {
	std::cout << "TCP server is started...." << std::endl;

	try{
		auto s = new TCPServer(DefaultPort);
		// Run the server for the specified time limit
		s->run(TimeLimit);
		delete s;
	}
	catch (std::exception &e) {
		std::cerr << "Exception occurred: " << e.what() << std::endl;
	}

	return 0;
}