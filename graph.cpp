//
// Created by ostapvad on 07.05.23.
//

#include "graph.h"

inline long long int squaredEuclideanDistance(const esw::Location& point1, const esw::Location& point2){
	long long int dx  = point1.x() - point2.x();
	long long int dy = point1.y() - point2.y();
	return dx*dx + dy*dy;
}

Graph::Graph():totalWalks(0), totalNodes(0){}

void Graph::AddWalk(const esw::Walk& walk){
	// Check if the walk is empty
	if(walk.locations().empty()) return;

	// Process the first location in the walk
	auto fromID = getVertexID(walk.locations(0));
	// If the location is not in graph add it with ID of totalNodes
	if(fromID == UNFOUND){
		setLocation(walk.locations(0), totalNodes);
		vertex_to_loc.push_back(walk.locations(0));
		fromID = totalNodes ++;
	}

	//Iterate over edges
	for (int i = 1; i < walk.locations_size(); i++) {
		auto toID = getVertexID(walk.locations(i));

		// If the location is not in graph add it with ID of totalNodes
		if (toID == UNFOUND){
			setLocation(walk.locations(i), totalNodes);
			vertex_to_loc.push_back(walk.locations(i));
			toID = totalNodes ++;
		}

		// Update the adjacency list with the walk information
		if (fromID == UNFOUND || toID == UNFOUND) { // edge is new
			adj_list[fromID][toID].totalWalks = 1;
			adj_list[fromID][toID].totalLength = walk.lengths(i - 1);
		} else { // edge is already in the graph
			adj_list[fromID][toID].totalWalks ++;
			adj_list[fromID][toID].totalLength += walk.lengths(i - 1);
		}
		fromID = toID;
	}

	std::cout << "Walks received = " << ++totalWalks << std::endl;
}

long long int Graph::computeDists(const esw::Location& source, esw::Location *target) {
	// Get the vertex ID of the source location
	auto sourceVertex = getVertexID(source);

	// Get the vertex ID of the target location (if provided)
	// choose between total length and shortest path length
	auto targetVertex = (target == nullptr) ? UNFOUND: getVertexID(*target);

	// Create a priority queue for Dijkstra's algorithm
	std::priority_queue<iPair, std::vector<iPair>, std::greater<>> pq;

	// Initialize distances to vertexes and visited arrays
	std::vector<long long int> dist(totalNodes, INF_LONG);
	std::vector<bool> isVisited(totalNodes, false);

	// total length from source to all other nodes
	long long int totalLength = 0LL;

	// Set the distance of the source vertex to 0 and mark it as visited
	dist[sourceVertex] = 0;
	pq.emplace(0, sourceVertex);

	while (!pq.empty()){
		// Get the current vertex from the priority queue
		auto cur_vertex = pq.top().second;
		pq.pop();
		if(isVisited[cur_vertex]) continue;
		// Update total length if the node is just proceeded
		totalLength += dist[cur_vertex];
		isVisited[cur_vertex] = true;

		// Process the adjacent vertices
		for(auto edge: adj_list[cur_vertex]){
			auto to_vertex = edge.first;

			// compute average edge weight
			long long int weight =  edge.second.totalLength / edge.second.totalWalks;
			long long int new_dist = dist[cur_vertex] + weight + 0LL;

			// If a shorter path is found and node is not visited, update the distance and total path
			if (!isVisited[to_vertex] && dist[to_vertex] > new_dist) {

				// Update distance to vertex
				dist[to_vertex] = new_dist;

				// If the target vertex is reached, return the distance
				if(to_vertex == targetVertex) return  dist[to_vertex];

				// Push the updated distance and vertex to the priority queue
				pq.push(std::make_pair(dist[to_vertex], to_vertex));
			}
		}
	}
	// Return the total length from source to all other
	return totalLength;
}



void Graph::print_adj_list() {
	std::string text;
	// iterate over adj list and save to text (used only for debugging purposes)
	for(const auto& edge: adj_list){
		auto from_vertex = edge.first;
		for(auto  to_vertex: edge.second){
			char buffer_first[100], buffer_second[100];
			std::sprintf(buffer_first, "from: %lld, to: %lld, total_length: %d, total_walks: %d ",
						 from_vertex, to_vertex.first, to_vertex.second.totalLength, to_vertex.second.totalWalks);
			std::sprintf(buffer_second, "(x_from, y_from) = (%d, %d), (x_to, y_to) = (%d, %d)\n",
						 vertex_to_loc[from_vertex].x(), vertex_to_loc[from_vertex].y(),
						 vertex_to_loc[to_vertex.first].x(), vertex_to_loc[to_vertex.first].y());
			std::string first_str(buffer_first);
			text += first_str;
			std::string second_str(buffer_second);
			text += second_str;


		}
	}
	std::cout << text << std::endl;
}

int Graph::getVertexID(const esw::Location &location) {
	// calculate approximate grid point in the map
	int x = location.x() / stepSize;
	int y = location.y() / stepSize;

	// iterate over nearest points, and point itself
	for(unsigned int j = 0; j < TOTAL_DIRS; j++) {
		int next_pose_x = x + dirs[j][0];
		int next_pose_y = y + dirs[j][1];
		// check if point is in grid and squared Euclidean distance between location and neighbour is less
		// or equal than threshold return vertexID of nearest neighbour
		if(next_pose_x >= 0 && next_pose_y >= 0 && next_pose_y < mapSize &&
				next_pose_x < mapSize && x_axis[next_pose_x] && y_axis[next_pose_y] &&
				squaredEuclideanDistance(location, vertex_to_loc[mapToVertexID[next_pose_x][next_pose_y]])
				<= SQUARE_THRESHOLD) return mapToVertexID[next_pose_x][next_pose_y];

		}
	// nearest neighbour was not found
	return UNFOUND;
}

void Graph::setLocation(const esw::Location &location,  int vertexID) {
	// update grid map, assign grid map coordinate to vertexID
	int x = location.x() / stepSize;
	int y = location.y() / stepSize;
	x_axis[x] = y_axis[y] = true;
	mapToVertexID[x][y] = vertexID;
}

