// Created by ostapvad on 06.05.23.
//

#ifndef ESW_EFFICIENT_SERVERS_TCPSERVER_H
#define ESW_EFFICIENT_SERVERS_TCPSERVER_H

#include "boost/asio.hpp"
#include "graph.h"
#include "session.h"
#include <boost/array.hpp>

/**
 * The TCPServer class represents a TCP server that asynchronously accepts client connections and handles sessions.
 */
class TCPServer {
public:
	/**
	 * Constructs a TCPServer object with the specified port.
	 *
	 * @param port The port number to listen on.
	 */
	explicit TCPServer(unsigned short port);

	/**
	 * Destructor for the TCPServer class.
	 */
	~TCPServer();

	/**
	 * Runs the server for the specified duration in seconds.
	 *
	 * @param totalSeconds The total duration to run the server.
	 */
	void run(unsigned int totalSeconds);

private:
	std::vector<std::shared_ptr<Session>> activeSessions; // Vector to store active sessions
	unsigned short port_; // The port number to listen on
	Graph* graph_; // Pointer to the Graph object
	boost::asio::io_context io_context_; // The IO context for asynchronous operations
	boost::asio::ip::tcp::acceptor acceptor_; // Acceptor for accepting client connections

	/**
	 * Accepts a client connection and starts a new session.
	 */
	void acceptConnection();
};



#endif //ESW_EFFICIENT_SERVERS_TCPSERVER_H